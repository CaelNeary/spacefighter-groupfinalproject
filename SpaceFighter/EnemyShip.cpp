
#include "EnemyShip.h"
#include "BioEnemyShip.h"
#include "Blaster.h"
#include "Level.h"
#include "Weapon.h"



EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);


}


void EnemyShip::Update(const GameTime* pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); //balance fps stopings things loading to fast/slow depending on computer hardware

		if (m_delaySeconds <= 0)
		{



			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
		
		
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;
	
	

	Ship::Initialize();
}


//takes damage done by projectile and adds it to EnmyShip
void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}

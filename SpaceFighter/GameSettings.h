#pragma once

//  cael
class GameSettings {

public:
	GameSettings::GameSettings() { 
		fPlayerShipSpeed = 300;
//		fEnemyShipSpeed = 300;
	}

	virtual float GetPlayerShipSpeed() const { return fPlayerShipSpeed; }
	virtual void SetPlayerShipSpeed(const float speed) { fPlayerShipSpeed = speed; }

	//virtual float GetEnemyShipSpeed() const { return fEnemyShipSpeed; }
	//virtual void SetEnemyShipSpeed(const float speed) { fEnemyShipSpeed = speed; }

private:
	float fPlayerShipSpeed;
//	float fEnemyShipSpeed;

};


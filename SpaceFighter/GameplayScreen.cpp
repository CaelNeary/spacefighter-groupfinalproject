
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "MenuItem.cpp"
#include <string>
#include "MainMenuScreen.h"
#include <MenuScreen.cpp>
#include "BioEnemyShip.h"



GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pTexture = nullptr;
	m_pLevel = nullptr;
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); break;
	}

	
	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);
	
	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);

	m_pTexture = pResourceManager->Load<Texture>("Textures\\background.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 10;

	

}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{

	pSpriteBatch->Begin();
	
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	

	pSpriteBatch->End();

	pSpriteBatch->Begin();
	m_pLevel->Draw(pSpriteBatch);
	pSpriteBatch->End();

	
}

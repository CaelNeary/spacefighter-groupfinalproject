
#include "BioEnemyShip.h"
#include "Blaster.h"
#include "Level.h"
#include "Weapon.h"
#include "EnemyShip.h"





BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150); //set how fast ships fly
	SetMaxHitPoints(1);//set how much "heath" the enemy ship has
	SetCollisionRadius(20); //Changes the size of the hit zone.
	


}

//Sets enemyship speed, postions, and time of game. If not on screen object is deacatived
void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		
		
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		
		
		
		
		if (!IsOnScreen()) Deactivate();

		
		
			
	}

	EnemyShip::Update(pGameTime);
}


//creates enemyship 
void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		m_BioEnemyShip = new BioEnemyShip();
		Blaster* pBlaster = new Blaster(true);
		pBlaster->SetProjectilePool(&m_projectiles);
		
		AttachWeapon(pBlaster, Vector2::UNIT_Y * -20);
		FireWeapons(TriggerType::PRIMARY);
		for (int i = 0; i < 2; i++)
		{

		Projectile* pProjectile = new Projectile();
//			m_projectiles.push_back(pProjectile);
			AddGameObject(pProjectile);


		}
		FireWeapons(TriggerType::PRIMARY);
		

		
	}
}

#pragma once

#include "Weapon.h"

class Phaser : public Weapon
{

public:

	Phaser(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 2;
		m_cooldownSeconds = 2.5; //Lower time means faster ammo output
	}

	virtual ~Phaser() { }

	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};